import { FETCH_TODOS, NEW_TODO } from './types';

export const fetchTodos = () => dispatch => {
    
    fetch('https://jsonplaceholder.typicode.com/todos')
        .then(res => res.json())
        .then(todos => dispatch({
            type: FETCH_TODOS,
            payload: todos
        }));
}