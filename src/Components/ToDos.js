import React, { Component } from 'react';
import ToDoItem from './ToDoItem';
import PropTypes from 'prop-types'; // ES6
import { connect } from 'react-redux';
import { fetchTodos } from '../actions/todoActions';
class ToDos extends Component {
    deleteToDo(id) {
        this.props.onDelete(id);
    }
    componentWillMount() {
        this.props.fetchTodos();
    }
    render() {
        
        const todoItems = this.props.todos.map((todo, index) => (
            <ToDoItem key={index} index={index} />
        ));
        let ToDoItems;
        // if (this.props.toDos) {
        //     ToDoItems = this.props.toDos.map(toDos => {
        //         return (
        //             <ToDoItem onDelete={this.deleteToDo.bind(this)} key={toDos.title} toDos={toDos} />
        //         );
        //     });
        // }

        return (
            <ul className="ToDos">
                <h3>lateToDoItems</h3>

                <h3>to do items from redux</h3>
                {todoItems}
            </ul>
        );
    }
}
ToDos.propTypes = {
    toDos: PropTypes.array,
    onDelete: PropTypes.func
}

const mapStateToProps = state => ({
    todos: state.todos.items
});
export default connect(mapStateToProps, { fetchTodos })(ToDos);