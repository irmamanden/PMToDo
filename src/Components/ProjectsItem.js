import React, { Component } from 'react';
import PropTypes from 'prop-types'; // ES6

class ProjectsItem extends Component {
  deleteProject(id) {
    this.props.onDelete(id);
  }
  render() {
    return (
      <li className="Project">
        <strong> {this.props.project.title} - {this.props.project.category}</strong><button  onClick={this.deleteProject.bind(this, this.props.project.id)}>x</button>

      </li>
    );
  }
}
ProjectsItem.propTypes = {
  project: PropTypes.object,
  onDelete: PropTypes.func
}
export default ProjectsItem;
