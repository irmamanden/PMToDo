import React, { Component } from 'react';
import ProjectItem from './ProjectsItem';
import PropTypes from 'prop-types'; // ES6

class Projects extends Component {
  deleteProject(id) {
    this.props.onDelete(id);
  }
  render() {
    let projectItems;
    if (this.props.projects) {
      projectItems = this.props.projects.map(project => {
        return (
          <ProjectItem onDelete={this.deleteProject.bind(this)} key={project.title} project={project} />
        );
      });
    }

    return (
      <ul className="Projects">
        <h3>latest projects</h3>
        {projectItems}
      </ul>
    );
  }
}
Projects.propTypes = {
  project: PropTypes.array,
  onDelete: PropTypes.func
}
export default Projects;
