import React, { Component } from 'react';
import PropTypes from 'prop-types'; // ES6

class AddTodo extends Component {
    constructor() {
        super();
        this.state = {
            newProject: {}
        };
    }
    static defaultProps = {
        categories: [
            "true", "false"
        ]
    }
    handleSubmit(e) {
        debugger
        if (this.refs.value === '') {
            alert('title is required');
        } else {
            this.setState({
                newTodo: {                    
                    title: this.refs.title.value,
                    completed: JSON.parse(this.refs.completed.value)
                }
            }, function () {
                this.props.addTodo(this.state.newTodo);
            })
        }

        e.preventDefault();
    }
    render() {
        let completedOptions = this.props.categories.map(completed => {
            return <option key={completed} value={completed}>{completed}</option>
        });
        return (
            <div >
                <h3>add todo </h3>

                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <label>title</label><br />
                        <input type="text" ref="title" />
                    </div>
                    <div>
                        <label>completed</label><br />
                        <select ref="completed">
                            {completedOptions}
                        </select>
                    </div>
                    <br />
                    <input type="submit" value="submit" />
                    <br />
                </form>
            </div>
        );
    }
}
AddTodo.propTypes = {
    addTodo: PropTypes.func,
    handleSubmit: PropTypes.func
}
export default AddTodo;
