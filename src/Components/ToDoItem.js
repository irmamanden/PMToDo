import React, { Component } from 'react';
import PropTypes from 'prop-types'; // ES6
import { connect } from 'react-redux';
import { fetchTodos } from '../actions/todoActions';
class ToDoItem extends Component {
    deleteToDo(id) {
        this.props.onDelete(id);
    }
    componentWillMount() {
        console.log("props", this.props)
    }
    render() {
        let propsTodos = this.props.todos[this.props.index];

        return (
            <li className="ToDo">
                to do item

          <strong> {propsTodos.title} - {propsTodos.completed.toString()}</strong> <button onClick={this.deleteToDo.bind(this, propsTodos.id)}>x</button>

            </li>
        );
    }
}
ToDoItem.propTypes = {
    toDo: PropTypes.object,
    onDelete: PropTypes.func
}
const mapStateToProps = state => ({
    todos: state.todos.items
});
export default connect(mapStateToProps, { fetchTodos })(ToDoItem);
