import React, { Component } from 'react';
import uuid from 'uuid';
import PropTypes from 'prop-types'; // ES6

class AddProject extends Component {
    constructor() {
        super();
        this.state = {
            newProject: {}
        };
    }
    static defaultProps = {
        categories: [
            "web design", "web development", "mob development"
        ]
    }
    handleSubmit(e) {
        if (this.refs.value === '') {
            alert('title is required');
        } else {
            this.setState({
                newProject: {
                    id: uuid.v4(),
                    title: this.refs.title.value,
                    category: this.refs.category.value
                }
            }, function () {

                this.props.addProject(this.state.newProject);
            })
        }

        e.preventDefault();

    }
    render() {
        let categoryOptions = this.props.categories.map(category => {
            return <option key={category} value={category}>{category}</option>
        });
        return (
            <div >
                <h3>add project </h3>

                <form onSubmit={this.handleSubmit.bind(this)}>
                    <div>
                        <label>title</label><br />
                        <input type="text" ref="title" />
                    </div>
                    <div>
                        <label>category</label><br />
                        <select ref="category">
                            {categoryOptions}
                        </select>
                    </div>
                    <br />
                    <input type="submit" value="submit" />
                    <br />
                </form>
            </div>
        );
    }
}
AddProject.propTypes = {
    categories: PropTypes.array,
    addProject: PropTypes.func,
    handleSubmit: PropTypes.func
}
export default AddProject;
