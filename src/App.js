import React, { Component } from 'react';
import './App.css';
import Projects from './Components/Projects';
import AddProject from './Components/AddProject';
import AddTodo from './Components/AddTodo';
import uuid from 'uuid';
import $ from "jquery";
import ToDos from './Components/ToDos';
import { Provider } from 'react-redux';
import store from './store';
class App extends Component {
  constructor() {
    super();
    this.state = {
      projects: []
    }
  }

  getToDos() {
    $.ajax({
      url: 'https://jsonplaceholder.typicode.com/todos',
      dataType: 'json',
      cache: false,
      success: function (data) {
        this.setState({ todos: data }, function () {
          console.log(this.state);
        });

      }.bind(this),
      error: function (xhr, status, error) {
        console.log(error)
      }
    })
  }

  handleDeleteToDo(id) {
    let todos = this.state.todos
    let index = todos.findIndex(x => x.id === id);
    todos.splice(index, 1);
    this.setState({ todos: todos });
  }

  handleToDo(toDo) {
    let todos = this.state.todos;
    todos.push(toDo);
    this.setState({ todos: todos });
    console.log('​handleToDo -> ', toDo);
  }

  getProjects() {
    this.setState({
      projects: [
        {
          id: uuid.v4(),
          title: 'buisness website',
          category: 'webdesign'
        },
        {
          id: uuid.v4(),
          title: 'social app',
          category: 'mob dev'
        },
        {
          id: uuid.v4(),
          title: 'e commerce',
          category: 'webdev'
        }
      ]
    });
  }

  componentDidMount() {
    this.getProjects();
    this.getToDos();
  }

  componentWillMount() {
    this.getToDos();
  }

  handleProject(project) {
    let projects = this.state.projects;
    projects.push(project);
    this.setState({ projects: projects });
  }

  handleDeleteProject(id) {
    console.log('​handleDeleteProject -> ', id);
    let projects = this.state.projects
    let index = projects.findIndex(x => x.id === id);
    projects.splice(index, 1);
    this.setState({ projects: projects });
  }

  render() {
    return (
      <div className="App">
        <AddProject addProject={this.handleProject.bind(this)} />

        <Projects projects={this.state.projects} onDelete={this.handleDeleteProject.bind(this)} />
        <hr />
        <AddTodo addTodo={this.handleToDo.bind(this)} />
        <Provider store={store}>
          <ToDos toDos={this.state.todos} onDelete={this.handleDeleteToDo.bind(this)} />
        </Provider>

      </div>
    );
  }
}

export default App;
